package com.yosafatwilly.training.microservice.catalog.dao;

import com.yosafatwilly.training.microservice.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
