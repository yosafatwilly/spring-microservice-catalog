package com.yosafatwilly.training.microservice.catalog.controller;

import com.yosafatwilly.training.microservice.catalog.dao.ProductDao;
import com.yosafatwilly.training.microservice.catalog.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping("api/product")
public class ProductApiController {

    @Autowired
    private ProductDao productDao;

    @GetMapping("/")
    public Page<Product> findProducts(Pageable page){
        return productDao.findAll(page);
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable("id") Product p){
        return p;
    }
}
